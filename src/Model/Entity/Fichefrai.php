<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Fichefrai Entity.
 *
 * @property int $id
 * @property int $mois
 * @property float $montantValide
 * @property \Cake\I18n\Time $dateAjout
 * @property \Cake\I18n\Time $dateModif
 * @property int $id_user
 * @property int $id_statut
 */
class Fichefrai extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
