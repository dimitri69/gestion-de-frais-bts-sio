<?php
namespace App\Model\Table;

use App\Model\Entity\Fichefrai;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Fichefrais Model
 *
 */
class FichefraisTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fichefrais');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Users', [
            'className' => 'Users',
            'foreignKey' => 'id_user',
        ]);
        $this->belongsTo('Statuts', [
            'className' => 'Statuts',
            'foreignKey' => 'id_statut',
        ]);

        $this->belongsTo('Ficheforfaits', [
            'className' => 'Ficheforfaits',
            'joinTable' => 'ficheforfaits',
            'targetForeignKey' => 'id_fichefrais'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->add('mois', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('mois');

        $validator
            ->add('montantValide', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('montantValide');

        $validator
            ->add('dateAjout', 'valid', ['rule' => 'date'])
            ->allowEmpty('dateAjout');

        $validator
            ->add('dateModif', 'valid', ['rule' => 'date'])
            ->allowEmpty('dateModif');

        $validator
            ->add('id_user', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id_user');

        $validator
            ->add('id_statut', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id_statut');

        return $validator;
    }

    public function getLesMoisDisponibles($id_user = null)
    {
        if ($id_user != null) {
            $res = $this->find("all", [
                'fields' => array('Fichefrais.mois'),
                'conditions' => array('Fichefrais.id_user' => $id_user)
            ]);
        } else {
            $res = $this->find("all", [
                'fields' => array('Fichefrais.mois')
            ]);
        }

        $labelMois = array(1 => "Janvier", 2 => "Février", 3 => "Mars", 4 => "Avril", 5 => "Mai", 6 => "Mai", 7 => "Juin", 8 => "Juillet", 9 => "Septembre", 10 => "Octobre", 11 => "Novembre", 12 => "Décembre");
        $lists_mois = array();
        foreach ($res as $row) {
            $mois = substr($row->mois, 4, 2);
            $annee = substr($row->mois, 0, 4);
            foreach ($labelMois as $mois_chiffre => $mois_lettres) {
                if ((int)$mois_chiffre == (int)$mois) {
                    $date_chiffres = $annee . $mois_chiffre;
                    $date_lettres = $mois_lettres . " " . $annee;
                    $lists_mois["$date_chiffres"] = "$date_lettres";
                }
            }
        }
        return $lists_mois;
    }

    public function getStatutFichefrais($id_user, $mois)
    {
        $res = $this->find("all", ['fields' => array('Statuts.libelle'), 'contain' => 'Statuts', 'conditions' => array('Fichefrais.id_user' => $id_user, 'Fichefrais.mois' => $mois)]);
        return $res->first();
    }

    public function getStatutFichefraisById($id_fichefrais)
    {
        $res = $this->find("all", ['fields' => array('Statuts.libelle'), 'contain' => 'Statuts', 'conditions' => array('Fichefrais.id' => $id_fichefrais)]);
        return $res->first();
    }

    public function getMontantValide($id_user, $mois)
    {
        $res = $this->find("all", ['fields' => array('Fichefrais.montantValide'), 'conditions' => array('Fichefrais.id_user' => $id_user, 'Fichefrais.mois' => $mois)]);
        return $res->first();
    }

    public function getMontantValideById($id_fichefrais)
    {
        $res = $this->find("all", ['fields' => array('Fichefrais.montantValide'), 'conditions' => array('Fichefrais.id' => $id_fichefrais)]);
        return $res->first();
    }

    public function getId($user_id, $mois)
    {
        $res = $this->find("all", ['fields' => array('Fichefrais.id'), 'conditions' => array('Fichefrais.id_user' => $user_id, 'Fichefrais.mois' => $mois)]);
        return $res->first();
    }

}
