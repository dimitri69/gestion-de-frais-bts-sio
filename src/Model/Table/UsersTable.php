<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('users');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Profiles');
        // $this->hasMany(array(
        //   'Fichefrais'=> array(
        //       'className' => 'Fichefrais',
        //       'foreignKey' => 'id_user'
        //    )
        // ));

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('nom');

        $validator
            ->allowEmpty('prenom');

        $validator
            ->allowEmpty('adresse');

        $validator
            ->allowEmpty('ville');

        $validator
            ->allowEmpty('cp');

        $validator
            ->add('dateEmbauche', 'valid', ['rule' => 'date'])
            ->allowEmpty('dateEmbauche');

        $validator
            ->allowEmpty('login');

        $validator
            ->allowEmpty('password');

        $validator
            ->add('id_profile', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id_profile');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['login']));
        return $rules;
    }
}
