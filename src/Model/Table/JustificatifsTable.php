<?php
namespace App\Model\Table;

use App\Model\Entity\Justificatif;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Justificatifs Model
 *
 */
class JustificatifsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('justificatifs');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Fraishorsforfaits', [
             'className' => 'Fraishorsforfaits',
             'foreignKey' => 'id_justificatif'
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('lien_fichier');

        $validator
            ->allowEmpty('titre');

        $validator
            ->allowEmpty('type_fichier');

        $validator
            ->add('id_fraishorsforfaits', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id_fraishorsforfaits');

        return $validator;
    }
}
