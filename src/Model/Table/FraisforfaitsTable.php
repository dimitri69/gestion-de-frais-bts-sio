<?php
namespace App\Model\Table;

use App\Model\Entity\Fraisforfait;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Fraisforfaits Model
 *
 */
class FraisforfaitsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fraisforfaits');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Types',[
                'className' => 'Types',
                'foreignKey' => 'id_types'
        ]);

        // $this->belongsTo('Ficheforfaits', [
        //      'className' => 'Ficheforfaits',
        //      'joinType' => 'INNER',
        //      'joinTable' => 'ficheforfaits',
        //      'foreignKey'  => array('id_fichefrais', 'id_fraisforfaits'),
        //      'targetForeignKey' => array('Fichefrais.id','fraisforfaits.id')
        // ]);

        // $this->hasMany('Ficheforfaits', [
        //      'className' => 'ficheforfaits',
        //      'foreignKey' => 'id_ficheforfaits'
        // ]);

        $this->belongsTo('Ficheforfaits', [
                'className'             => 'Ficheforfaits',
                'joinTable'             => 'ficheforfaits',
                'targetForeignKey' => 'id_frais_forfaits'
        ]);

        //$this->hasMany('Ficheforfaits');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('description');

        $validator
            ->add('date_debut', 'valid', ['rule' => 'date'])
            ->allowEmpty('date_debut');

        $validator
            ->add('date_fin', 'valid', ['rule' => 'date'])
            ->allowEmpty('date_fin');

        $validator
            ->add('id_types', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id_types');

        return $validator;
    }

    /**
     * Retourne les mois pour lesquel un visiteur a une fiche de frais
     * @param $id_user
     * @return un tableau associatif de clé un mois -aaaamm- et de valeurs l'année et le mois correspondant
    */
      public function getLesMoisDisponibles($id_user){
        $res = $this->find("all", ['fields'=>array('Fichefrais.mois'), 'conditions' => array('Fichefrais.id_user'=>$id_user)]);
        $labelMois = array(01=>"Janvier",02=>"Février",03=>"Mars",04=>"Avril",05=>"Mai",06=>"Mai",07=>"Juin",08=>"Juillet",09=>"Septembre",10=>"Octobre",11=>"Novembre",12=>"Décembre");
        $lists_mois = array();
        foreach ($res as $row) {
          $mois = substr($row['Fichefrais']['mois'],4,2);
          $annee = substr($row['Fichefrais']['mois'],0,4);
          foreach ($labelMois as $mois_chiffre => $mois_lettres) {
            if((int)$mois_chiffre == (int)$mois){
              $date_chiffres = $mois_chiffre."-".$annee;
              $date_lettres = $mois_lettres." ".$annee;
              $lists_mois["$date_chiffres"]="$date_lettres";
            }
          }
        }
        return $lists_mois;
      }

}
