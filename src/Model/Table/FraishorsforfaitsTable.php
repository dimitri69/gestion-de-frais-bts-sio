<?php
namespace App\Model\Table;

use App\Model\Entity\Fraishorsforfait;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Fraishorsforfaits Model
 *
 */
class FraishorsforfaitsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fraishorsforfaits');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Fichefrais', [
             'className' => 'Fichefrais',
             'foreignKey' => 'id_fichefrais'
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->add('date', 'valid', ['rule' => 'date'])
            ->allowEmpty('date');

        $validator
            ->add('montant', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('montant');

        $validator
            ->allowEmpty('libelle');

        $validator
            ->add('nb_justificatifs', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('nb_justificatifs');

        $validator
            ->add('id_fichefrais', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id_fichefrais');

        return $validator;
    }

    public function getNbJustifs($user_id, $mois){
      $res = $this->find("all", ['fields'=>array('Fraishorsforfaits.nb_justificatif'), 'contain'=>'Fichefrais', 'conditions' => array('Fichefrais.id_user'=>$user_id, 'Fichefrais.mois'=>$mois)])->toArray();
      $nb_justifs = 0;
      foreach ($res as $value) {
        $nb_justifs = $nb_justifs + $value['nb_justificatif'];
      }
      return $nb_justifs;
    }
    public function getNbJustifsById($id_fichefrais){
      $res = $this->find("all", ['fields'=>array('Fraishorsforfaits.nb_justificatif'), 'contain'=>'Fichefrais', 'conditions' => array('Fichefrais.id'=>$id_fichefrais)])->toArray();
      $nb_justifs = 0;
      foreach ($res as $value) {
        $nb_justifs = $nb_justifs + $value['nb_justificatif'];
      }
      return $nb_justifs;
    }
}
