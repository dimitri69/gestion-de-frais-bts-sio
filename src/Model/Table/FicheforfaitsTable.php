<?php
namespace App\Model\Table;

use App\Model\Entity\Ficheforfait;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Ficheforfaits Model
 *
 */
class FicheforfaitsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('ficheforfaits');
        $this->displayField('id_fichefrais');
        $this->primaryKey(['id_fichefrais', 'id_fraisforfaits']);


        $this->belongsTo('Fichefrais', [
             'className' => 'Fichefrais',
             'foreignKey' => 'id_fichefrais',
             'targetForeignKey' => 'id'
        ]);

        $this->belongsTo('Fraisforfaits', [
             'className' => 'Fraisforfaits',
             'foreignKey' => 'id_fraisforfaits',
             'targetForeignKey' => 'id'
        ]);



    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('quantite', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('quantite');

        $validator
            ->add('id_fichefrais', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id_fichefrais', 'create');

        $validator
            ->add('id_fraisforfaits', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id_fraisforfaits', 'create');

        return $validator;
    }
}
