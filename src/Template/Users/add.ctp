<div class="users form large-10 medium-9 columns content">
    <?= $this->Form->create($users) ?>
    <fieldset>
        <legend><?= __('Ajouter un utilisateur') ?></legend>
        <?php
            echo $this->Form->input('nom');
            echo $this->Form->input('prenom');
            echo $this->Form->input('adresse');
            echo $this->Form->input('ville');
            echo $this->Form->input('cp');
            echo $this->Form->input('dateEmbauche', ['empty' => true]);
            echo $this->Form->input('login');
            echo $this->Form->input('password');
            echo $this->Form->input('id_profiles');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Enregistrer')) ?>
    <?= $this->Form->end() ?>
</div>
