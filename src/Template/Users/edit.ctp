<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Supprimer l\'utilisateur'),
                ['action' => 'delete', $user->id],
                ['confirm' => __('Etes-vous sur de vouloir supprimer l\'utilisateur # {0}?', $user->id)]
            )
        ?></li>
    </ul>
</nav>
<div class="users form large-10 medium-9 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Edit User') ?></legend>
        <?php
            echo $this->Form->input('nom');
            echo $this->Form->input('prenom');
            echo $this->Form->input('adresse');
            echo $this->Form->input('ville');
            echo $this->Form->input('cp');
            echo $this->Form->input('dateEmbauche', ['empty' => true]);
            echo $this->Form->input('login');
            echo $this->Form->input('mdp');
            echo $this->Form->input('id_profile');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Enregistrer')) ?>
    <?= $this->Form->end() ?>
</div>
