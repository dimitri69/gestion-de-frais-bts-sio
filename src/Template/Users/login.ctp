<div class="fichefrais form large-12 medium-11 columns content">
  <?= $this->Flash->render('auth') ?>
  <?= $this->Form->create() ?>
  <fieldset style="width: 30%;margin: auto;">
    <legend><?= __('Identification utilisateur') ?></legend>
    <?= $this->Form->input('login') ?>
    <?= $this->Form->input('password') ?>
  <?= $this->Form->button('Login') ?>
  <?= $this->Form->end() ?>
  </fieldset>
</div>
