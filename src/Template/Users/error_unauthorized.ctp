<div class="fichefrais form large-10 medium-9 columns content">
  <?= $this->Flash->render('auth') ?>
  <h3>Problème d'accès à cette page.</h3><br>
  <h4>Vous n'êtes pas autoriser à accéder à cette page, compte tenu de vos droits.</h4><br><br>
  <a href="" onClick="javascript:window.history.go(-1)">Revenir à la page précédente</a><br>
</div>
