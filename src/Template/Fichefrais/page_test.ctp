<style>
    .ui-datepicker-calendar{
        table-layout: auto!important;
        width: auto!important;
    }
</style>
<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.4.1/jsgrid.min.css" />
<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.4.1/jsgrid-theme.min.css" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.4.1/jsgrid.min.js"></script>

<div class="fichefrais view large-10 medium-9 columns content">

    <select name="date_select" id="date_select" class="form-control">
        <option value="20161">Janvier 2016</option>
        <option value="201512">Décembre 2015</option>
        <option value="201511">Novembre 2015</option>
        <option value="201510">Octobre 2015</option>
    </select>

    <div id="block-alphabet">
        <?php
        for($lettre='A'; $lettre != 'AA'; $lettre++)
        {
            echo $this->Html->link($lettre, ['controller' => 'Fichefrais', 'action' => 'validationFrais', $lettre], ['class' => 'alphabet']);
        }
        ?>
    </div>
    <div id="resultat">
        <table cellpadding="0" cellspacing="0">
            <thead>
            <tr>
                <th><?= __('Nom') ?></th>
                <th><?= __('Prénom') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($list_utilisateurs as $utilisateur): ?>
                <tr data-toggle="modal" data-target="#myModal" id="<?= $utilisateur['id']?>">
                    <td><?= $utilisateur['nom'] ?></td>
                    <td><?= $utilisateur['prenom'] ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width: 950px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Validation du frais</h4>
            </div>
            <div class="modal-body">
                <div id="jsGrid"></div>
            </div>
            <div class="modal-footer">
                <button type="button" id="valider" class="btn btn-primary">Valider</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
            </div>
        </div>
    </div>
</div>


<script>

    $('tr').click(function(){
        var user_id = $(this).attr('id');
        var mois = $("#date_select").val();
        var url = '/fichefrais/ajaxdetailsValidation/'+user_id+'/'+mois;
        //$(".modal-body").html('<div style="text-align:center; margin-right:auto; margin-left:auto">Patientez...<br><br><br><br><img src="../../img/ajax-loader.gif"/></div>');
        $.ajax({
                method: "get",
                url: url
            })
            .done(function(data) {
                //$(".modal-body").html(data).show();
            })
    });

    var MyDateField = function(config) {
        jsGrid.Field.call(this, config);
    };

    MyDateField.prototype = new jsGrid.Field({
        sorter: function(date1, date2) {
            return new Date(date1) - new Date(date2);
        },
        itemTemplate: function(value) {
            var datetimeNow = new Date(value);
            return datetimeNow.getDate() + "/" + (datetimeNow.getMonth()+1) + "/" + datetimeNow.getFullYear();
        },
        insertTemplate: function(value) {
            return this._insertPicker = $("<input>").datepicker({ defaultDate: new Date() });
        },
        editTemplate: function(value) {
            return this._editPicker = $("<input>").datepicker().datepicker("setDate", new Date(value));
        },
        insertValue: function() {
            return this._insertPicker.datepicker("getDate").getFullYear() + "-" + (this._insertPicker.datepicker("getDate").getMonth()+1) + "-" + this._insertPicker.datepicker("getDate").getDate();
        },
        editValue: function() {
            return this._editPicker.datepicker("getDate").getFullYear() + "-" + (this._editPicker.datepicker("getDate").getMonth()+1) + "-" + this._editPicker.datepicker("getDate").getDate();
        }
    });

    jsGrid.fields.myDateField = MyDateField;

    var types = [
        { Name: "Forfait Etape", Id: 1 },
        { Name: "Nuitée Hôtel", Id: 2 },
        { Name: "Frais Killométriques", Id: 3 },
        { Name: "Repas Restaurants", Id: 4 }
    ];

    $("#jsGrid").jsGrid({
        width: "100%",
        height: "auto",

        autoload: true,

        controller: {
            loadData: function() {
                return $.ajax({
                    type: "GET",
                    url: "./jsGrid",
                    dataType: "json"
                });
            },
            updateItem: function(item) {
                return $.ajax({
                    type: "PUT",
                    url: "./jsGrid",
                    data: item,
                    dataType: "json"
                });
            },
            insertItem: function(item) {
                return $.ajax({
                    type: "POST",
                    url: "./jsGrid",
                    data: item,
                    dataType: "json"
                });
            },
            deleteItem: function(item) {
                return $.ajax({
                    type: "DELETE",
                    url: "./jsGrid",
                    data: item,
                    dataType: "json"
                });
            }
        },

        inserting: true,
        editing: true,
        sorting: true,
        paging: false,
        pageLoading: false,
        loadIndication: true,
        loadMessage: "Chargement en cours...",
        confirmDeleting: true,
        deleteConfirm: "Etes-vous sûr de vouloir supprimer cet élément ?",
        updateOnResize: true,
        noDataContent: "Pas de frais forfaitisés trouvés",

        //data: <?//=$data_forfaits?>,

        fields: [
            { name: "ID", visible: false},
            { name: "Type", type: "select", align: "center", items: types, valueField: "Id", textField: "Name"},
            { name: "Description", align: "center", type: "textarea" },
            { name: "Debut", align: "center", type: "myDateField", width: "55" },
            { name: "Fin", align: "center", type: "myDateField", width: "55"},
            { name: "Quantite", align: "center", type: "number", width: "30" },
            { name: "PU", align: "center", width: "10" },
            { name: "Montant", align: "center", width: "25",itemTemplate: function(value) {
                return value.toFixed(2) + " €"; } },
            { type: "control", align: "center", width: "20" }
        ]
    });
</script>