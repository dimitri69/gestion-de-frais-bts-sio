<div class="fichefrais form large-10 medium-9 columns content" style="background-color: #77AADD;">
  <h4>Saisie fiche de frais du mois de <?= date('Yn')?></h4>
  <fieldset>
      <legend><?= __('Elements forfaitisés') ?></legend>
      <div id="contenu_table_forfaits" style="overflow-x: hidden;overflow-y: auto;height: 315px; border-bottom: 2px solid #1798A5;">
        <?=$this->element('../Fichefrais/elements/table_forfaits', array($fichesforfaits));?>
      </div>
      <div><br><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <a id="add_forfait" href="">Ajouter un élément forfaitisé</a></div>
  </fieldset>

  <fieldset>
      <legend><?= __('Elements hors forfait') ?></legend>
      <div class="fichefrais form large-7 medium-6 columns">
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th><?= __('Actions') ?></th>
                    <th><?= __('Date') ?></th>
                    <th><?= __('Libelle') ?></th>
                    <th><?= __('Justificatifs') ?></th>
                    <th><?= __('Montant') ?></th>
                </tr>
            </thead>
            <tbody id="table_horsforfait">
                <?php $total_horsforfait = 0; ?>
                <?php foreach ($fraishorsforfaits as $fraishorsforfait): ?>
                  <tr>
                      <td><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></td>
                      <td><?= date("d/m/Y", strtotime($fraishorsforfait['date']))?></td>
                      <td><?=$fraishorsforfait['libelle']?></td>
                      <td>
                        <?php if($fraishorsforfait['justificatifs']['lien_fichier'] != null): ?>
                          <a href="<?=$fraishorsforfait['justificatifs']['lien_fichier']?>" title="<?=$fraishorsforfait['justificatifs']['titre']?>"><span class="glyphicon glyphicon-file" aria-hidden="true"></span> <?=$fraishorsforfait['justificatifs']['titre']?></a></td>
                        <?php endif; ?>
                      <td><?=$fraishorsforfait['montant']?></td>
                  </tr>
                  <?php $total_horsforfait = $total_horsforfait + $fraishorsforfait['montant']; ?>
                <?php endforeach; ?>
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td><strong>TOTAL</strong></td>
                  <td><strong><?= $total_horsforfait ?> €</strong></td>
                </tr>
            </tbody>
        </table>
      </div>
      <div class="fichefrais form large-5 medium-4 columns" style="background-color: #fff;">
        <fieldset style="margin-top: 5px;">
          <legend><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <?= __('Nouvel élements hors forfait') ?></legend>
          <div class="input-group">
            <form method="post" enctype="multipart/form-data" action="../fichefrais/add">
              <label for="libelle_horsforfait">Libellé : </label>
              <input type="text" id="libelle_horsforfait" name="libelle_horsforfait" class="form-control" placeholder="Intitulé du frais hors-forfait">
              <label for="date_horsforfait">Date : </label>
              <input type="date" id="date_horsforfait" name="date_horsforfait" class="form-control">
              <label for="montant_horsforfait">Montant : </label>
              <input type="number" step="0.01" placeholder="10.99" id="montant_horsforfait" name="montant_horsforfait" class="form-control">
              Choisir un justificatif (.pdf/.doc/.docx) : <input name="Justificatif_horsforfait" type="file" />
              <input type="text" id="titre_justif_horsforfait" name="titre_justif_horsforfait" placeholder="Intitulé du justificatif" class="form-control">
              <input type="submit" value="Ajouter" />
            </form>
          </div>
        </fieldset>
      </div>
  </fieldset>
</div>

<script>
    $('#add_forfait').click(function(e){
    e.preventDefault();
    $('#table_forfait').append('<tr><td><?=$this->Form->input('types')?></td><td><textarea id="description_forfait" type="text" class="form-control" placeholder=""></textarea></td><td style="padding: 0 10px;"><input type="text" id="date_debut_forfait" name="date_debut_forfait" value="<?=date('%d/%m/%Y')?>" class="form-control" placeholder=""></td><td style="padding: 0 10px;"><input type="text" id="date_fin_forfait" name="date_fin_forfait" value="<?=date('%d/%m/%Y')?>" class="form-control" placeholder=""></td><td><input type="number" id="qte_forfait" class="form-control" placeholder=""></td><td></td><td><button id="save_forfait" style="padding:0;">OK</button></td></tr>');

    $(function() {
        $('input[name="date_debut_forfait"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
        $('input[name="date_fin_forfait"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
    });

    $('#save_forfait').click(function(){
        var type_forfait = $('#types').val();
        var description_forfait = $("#description_forfait").val();
        var date_debut_forfait = $('input[name="date_debut_forfait"]').val();
        var date_fin_forfait = $('input[name="date_fin_forfait"]').val();
        //console.log(date_debut_forfait.val());
        var qte_forfait = $("#qte_forfait").val();

        var url = '../fichefrais/addForfait/';

        $("#contenu_table_forfaits").html('<div style="text-align:center; margin-right:auto; margin-left:auto">Patientez...<br><br><br><br><img src="../../img/ajax-loader.gif"/></div>');
        $.ajax({
          method: "post",
          url: url,
          data:{
              type_forfait: type_forfait,
              description_forfait: description_forfait,
              date_debut_forfait: date_debut_forfait,
              date_fin_forfait: date_fin_forfait,
              qte_forfait: qte_forfait
          }
        })
        .done(function(data) {
          $("#contenu_table_forfaits").html(data).show();
        })
    });
});
</script>
