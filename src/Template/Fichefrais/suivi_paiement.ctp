<style>
.alphabet{
  width: 8px;
  padding: 10px 31px 10px 20px;
  background: #f0f0f0;
  display: inline-block;
}
#block-alphabet{
  text-align: center;
}
#resultat{
  margin-top: 50px;
}
tr:hover{
  cursor: pointer;
  background-color: grey !important;
  color:white;
}
.modal-header {
    padding: 15px 15px 0;
}
button{
  width: 70px;
}
</style>
<div class="fichefrais view large-10 medium-9 columns content" style="background-color: #EE8855;">
  <div id="resultat">
    <table cellpadding="0" cellspacing="0">
      <thead>
          <tr>
              <th><?= __('Nom') ?></th>
              <th><?= __('Prénom') ?></th>
              <th><?= __('Mois') ?></th>
              <th><?= __('Statut') ?></th>
          </tr>
      </thead>
      <tbody>
      <?php foreach($fichefraisvalides as $fichefraisvalide): ?>
          <tr data-toggle="modal" data-target="#myModal" id="<?= $fichefraisvalide['id']?>">
              <td><?= $fichefraisvalide['user']['nom'] ?></td>
              <td><?= $fichefraisvalide['user']['prenom'] ?></td>
              <td><?= $fichefraisvalide['mois'] ?></td>
              <td><?= $fichefraisvalide['statut']['libelle'] ?></td>
          </tr>
      <?php endforeach; ?>
      </tbody>
    </table>
  </div>

    <div id="fichefrais">

    </div>

</div>

<script>
  $('tr').click(function(){
    var id_fichefrais = $(this).attr('id');
    var url = '/fichefrais/ajaxdetailsPaiment/'+id_fichefrais;
    $("#fichefrais").html('<div style="text-align:center; margin-right:auto; margin-left:auto">Patientez...<br><br><br><br><img src="../../img/ajax-loader.gif"/></div>');
    $.ajax({
      method: "get",
      url: url
    })
    .done(function(data) {
      $("#fichefrais").html(data).show();
    })
  });
</script>