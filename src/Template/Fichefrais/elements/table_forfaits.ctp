<style>
input, textarea{
  margin: 0 0 0 0 !important;
}
</style>
<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th style="width: 90px; text-align: center;"><?= __('Type') ?></th>
            <th style="width: 235px; text-align: center;"><?= __('Description') ?></th>
            <th style="width: 86px; text-align: center;"><?= __('Date de début') ?></th>
            <th style="width: 86px; text-align: center;"><?= __('Date de fin') ?></th>
            <th style="width: 85px; text-align: center;"><?= __('Quantité') ?></th>
            <th style="width: 70px; text-align: center;"><?= __('Montant') ?></th>
            <th style="width: 75px; text-align: center;"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody id="table_forfait">

    </tbody>
    <tbody>
        <?php $total_forfait = 0; ?>
        <?php foreach ($fichesforfaits as $fraisforfait): ?>
          <tr>
              <td style="text-align: center;"><?= $fraisforfait['types']['libelle']?></td>
              <td style="text-align: center;"><?=$fraisforfait['fraisforfaits']['description']?></td>
              <td style="text-align: center;"><?= date("d/m/Y", strtotime($fraisforfait['fraisforfaits']['date_debut']))?></td>
              <td style="text-align: center;"><?= date("d/m/Y", strtotime($fraisforfait['fraisforfaits']['date_fin']))?></td>
              <td style="text-align: center;"><?=$fraisforfait['quantite']." x ".$fraisforfait['types']['montant']. ' €'?></td>
              <?php $total_ligne = $fraisforfait['quantite']*$fraisforfait['types']['montant']; ?>
              <td style="text-align: center;"><?=$total_ligne.' €'?></td>
              <td style="text-align: center;"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></td>
          </tr>
          <?php $total_forfait = $total_forfait + $total_ligne; ?>
        <?php endforeach; ?>
        <div style="float:right;"><strong>TOTAL : <?= $total_forfait ?> €</strong></div>
    </tbody>
</table>