<div class="fichefrais index large-10 medium-9 columns content" style="background-color: #77AADD;">
    <h3><?= __('Fichefrais') ?></h3>

    <fieldset id="filtre_date">
      <legend><?= __('Choisir une date') ?></legend>
        <?php
          echo $this->Form->create(null, [ 'url' => '/fichefrais/index/', 'type' => 'post' ]);
          if(!empty($list_date)){
            echo $this->Form->input('date', array('options' => $list_date, 'default'=>$mois));
          }
          echo $this->Form->button(__('Enregistrer'));
          echo $this->Form->end();
        ?>
    </fieldset>

    <fieldset>
      <legend><?= __('Récapitulatif des frais pour le mois de ').$mois ?></legend>
      <div class="alert-success">Etat : <?=$statut_fiche_frais['Statuts']['libelle']?><br>Montant validé : <?=$montantValide_fiche_frais['Fichefrais']['montantValide']?> €</div>
      <table cellpadding="0" cellspacing="0">
          <legend><?= __('Eléments forfaitisés') ?></legend>
          <thead>
              <tr>
                  <th><?= __('Frais étapes') ?></th>
                  <th><?= __('Frais killométriques') ?></th>
                  <th><?= __('Nuitées Hotêls') ?></th>
                  <th><?= __('Repas Restaurants') ?></th>
              </tr>
          </thead>
          <tbody>
              <tr>
                  <td><?=(isset($donnees_forfaits['etape']['qte']) ? $donnees_forfaits['etape']['qte'] : '0') ?></td>
                  <td><?=(isset($donnees_forfaits['km']['qte']) ? $donnees_forfaits['km']['qte'] : '0')?> km</td>
                  <td><?=(isset($donnees_forfaits['hotel']['qte']) ? $donnees_forfaits['hotel']['qte'] : '0')?> nuits</td>
                  <td><?=(isset($donnees_forfaits['repas']['qte']) ? $donnees_forfaits['repas']['qte'] : '0')?> repas</td>
              </tr>
          </tbody>
      </table>

      <table cellpadding="0" cellspacing="0">
          <thead>
              <tr>
                  <th><?= __('Date') ?></th>
                  <th><?= __('Libelle') ?></th>
                  <th><?= __('Justificatifs') ?></th>
                  <th><?= __('Montant') ?></th>
              </tr>
          </thead>
          <tbody id="table_horsforfait">
              <?php $total_horsforfait = 0; ?>
              <?php foreach ($fraishorsforfaits as $fraishorsforfait): ?>
                <tr>
                    <td><?= date("d/m/Y", strtotime($fraishorsforfait['date']))?></td>
                    <td><?=$fraishorsforfait['libelle']?></td>
                    <td>
                      <?php if($fraishorsforfait['justificatifs']['lien_fichier'] != null): ?>
                        <a target="_blank" href="<?=$fraishorsforfait['justificatifs']['lien_fichier']?>" title="<?=$fraishorsforfait['justificatifs']['titre']?>"><span class="glyphicon glyphicon-file" aria-hidden="true"></span> Lien</a>
                      <?php endif; ?>
                    </td>
                    <td><?=$fraishorsforfait['montant']?></td>
                </tr>
                <?php $total_horsforfait = $total_horsforfait + $fraishorsforfait['montant']; ?>
              <?php endforeach; ?>
              <tr>
                <td></td>
                <td></td>
                <td><strong>TOTAL</strong></td>
                <td><strong><?= $total_horsforfait ?> €</strong></td>
              </tr>
          </tbody>
      </table>
      <button type="button" class="btn btn-default">
        Vue graphique
      </button>
      <button type="button" class="btn btn-default">
        Exporter en PDF
      </button>
      <button type="button" onclick="imprimer_page()" class="btn btn-default">
        Imprimer
      </button>
    </fieldset>
</div>
<script type="text/javascript">
function imprimer_page(){
  window.print();
}
</script>
