<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $fichefrai->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $fichefrai->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Fichefrais'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="fichefrais form large-9 medium-8 columns content">
    <?= $this->Form->create($fichefrai) ?>
    <fieldset>
        <legend><?= __('Edit Fichefrai') ?></legend>
        <?php
            echo $this->Form->input('mois');
            echo $this->Form->input('montantValide');
            echo $this->Form->input('dateAjout', ['empty' => true]);
            echo $this->Form->input('dateModif', ['empty' => true]);
            echo $this->Form->input('id_user');
            echo $this->Form->input('id_statu');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
