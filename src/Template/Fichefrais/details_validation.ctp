<style>
    .datepicker table {
        table-layout: auto !important;
        width: auto !important;
    }
</style>
<div class="fichefrais view large-10 medium-9 columns content" style="background-color: #EE8855;">

    <div id="header_valid_fiche">

        <h1><?= __('Récapitulatif des frais pour le mois de ') . $mois ?></h1>

        <div class="alert-success">Etat : <?= $statut_fiche_frais['Statuts']['libelle'] ?><br>Montant validé
            : <?= $montantValide_fiche_frais['Fichefrais']['montantValide'] ?> €
        </div>

        <div id="id_fichefrais" style="display:none;"><?= $id_fichefrais->Fichefrais['id'] ?></div>

    </div>

    <div id="frais_forfaits">
        <h2><?= __('Frais au forfait') ?></h2>
        <div id="jsGrid"></div>
    </div>

    <div id="frais_hors_forfaits">
        <h2><?= __('Hors Forfait') ?></h2>
        <table cellpadding="0" cellspacing="0">
            <legend><?= $nb_justifs ?> <?= __(' justificatifs reçus') ?></legend>
            <thead>
            <tr>
                <th><?= __('Date') ?></th>
                <th><?= __('Libelle') ?></th>
                <th><?= __('Montant') ?></th>
                <th><?= __('Justificatif') ?></th>
                <th><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php $total_montant = 0 ?>
            <?php foreach ($fraishorsforfaits as $frais): ?>
                <?php
                if (substr($frais->libelle, 0, 9) == "REFUSE - ") {
                    $statut_color = '#f2dede';
                    $option_selected = 'selected';
                    $refuse = true;
                } else {
                    $statut_color = 'none';
                    $option_selected = '';
                    $refuse = false;
                }
                ?>
                <tr style="background-color:<?= $statut_color ?>">
                    <td><?= date("d/m/Y", strtotime($frais->date)) ?></td>
                    <td><?= h($frais->libelle) ?></td>
                    <td><?= h($frais->montant) ?> €</td>
                    <td>
                        <?php if ($frais['justificatifs']['lien_fichier'] != null): ?>
                        <a href="<?= $frais['justificatifs']['lien_fichier'] ?>"
                           title="<?= $frais['justificatifs']['titre'] ?>"><span class="glyphicon glyphicon-file"
                                                                                 aria-hidden="true"></span> <?= $frais['justificatifs']['titre'] ?>
                        </a></td>
                    <?php endif; ?>
                    </td>
                    <td>
                        <select class="choix_horsforfait" name="choix_horsforfait">
                            <option value="valider">Valider</option>
                            <option <?= $option_selected ?> value="refuser">Refuser</option>
                            <option value="reporter">Reporter</option>
                        </select>
                        <?php
                        if(!empty($list_statuts)){
                            echo $this->Form->input('choix_horsforfait', array('options' => $list_statuts, 'default'=>$statut, 'id'=>($frais->id)));
                        }
                        ?>
                    </td>
                </tr>
                <?php
                if (!$refuse) {
                    $total_montant = $total_montant + $frais->montant;
                }
                ?>
            <?php endforeach; ?>
            <tr>
                <td>Montant à valider :</td>
                <td></td>
                <td id="montantValide"><?= $total_montant ?></td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">

    var MyDateField = function (config) {
        jsGrid.Field.call(this, config);
    };

    MyDateField.prototype = new jsGrid.Field({
        sorter: function (date1, date2) {
            return new Date(date1) - new Date(date2);
        },
        itemTemplate: function (value) {
            var datetimeNow = new Date(value);
            return datetimeNow.getDate() + "/" + (datetimeNow.getMonth() + 1) + "/" + datetimeNow.getFullYear();
        },
        insertTemplate: function (value) {
            return this._insertPicker = $("<input>").datepicker({defaultDate: new Date()});
        },
        editTemplate: function (value) {
            return this._editPicker = $("<input>").datepicker().datepicker("setDate", new Date(value));
        },
        insertValue: function () {
            return this._insertPicker.datepicker("getDate").getFullYear() + "-" + (this._insertPicker.datepicker("getDate").getMonth() + 1) + "-" + this._insertPicker.datepicker("getDate").getDate();
        },
        editValue: function () {
            return this._editPicker.datepicker("getDate").getFullYear() + "-" + (this._editPicker.datepicker("getDate").getMonth() + 1) + "-" + this._editPicker.datepicker("getDate").getDate();
        }
    });

    jsGrid.fields.myDateField = MyDateField;

    var types = [
        {Name: "Forfait Etape", Id: 1},
        {Name: "Nuitée Hôtel", Id: 2},
        {Name: "Frais Killométriques", Id: 3},
        {Name: "Repas Restaurants", Id: 4}
    ];

    $("#jsGrid").jsGrid({
        width: "100%",
        height: "auto",

        autoload: true,

        controller: {
            loadData: function () {
                return $.ajax({
                    type: "GET",
                    url: "../../jsGrid/<?=$user_id?>/<?=$mois?>",
                    dataType: "json"
                });
            },
            updateItem: function (item) {
                return $.ajax({
                    type: "PUT",
                    url: "../../jsGrid/<?=$user_id?>/<?=$mois?>",
                    data: item,
                    dataType: "json"
                });
            },
            insertItem: function (item) {
                return $.ajax({
                    type: "POST",
                    url: "../../jsGrid/<?=$user_id?>/<?=$mois?>",
                    data: item,
                    dataType: "json"
                });
            },
            deleteItem: function (item) {
                return $.ajax({
                    type: "DELETE",
                    url: "../../jsGrid/<?=$user_id?>/<?=$mois?>",
                    data: item,
                    dataType: "json"
                });
            }
        },

        inserting: true,
        editing: true,
        sorting: true,
        paging: false,
        pageLoading: false,
        loadIndication: true,
        loadMessage: "Chargement en cours...",
        confirmDeleting: true,
        deleteConfirm: "Etes-vous sûr de vouloir supprimer cet élément ?",
        updateOnResize: true,
        noDataContent: "Pas de frais forfaitisés trouvés",

        fields: [
            {name: "ID", visible: false},
            {name: "Type", type: "select", align: "center", items: types, valueField: "Id", textField: "Name"},
            {name: "Description", align: "center", type: "textarea"},
            {name: "Debut", align: "center", type: "myDateField", width: "55"},
            {name: "Fin", align: "center", type: "myDateField", width: "55"},
            {name: "Quantite", align: "center", type: "number", width: "30"},
            {name: "PU", align: "center", width: "10"},
            {
                name: "Montant", align: "center", width: "30", itemTemplate: function (value) {
                return value.toFixed(2) + " €";
            }
            },
            {type: "control", align: "center", width: "30"}
        ]
    });
</script>

<script>
    $(".choix_horsforfait").change(function () {
        alert('test');
        var choix_statut = $('#statut').val();
        //var id_fichefrais = $('#id_fichefrais').text();
        var id_fichefrais = $('#id_fichefrais').prop('id');

        var url = '/fichefrais/ajaxMisePaiement/';
        $.ajax({
                method: "post",
                url: url,
                data: {choix_statut: choix_statut, id_fichefrais: id_fichefrais}
            })
            .done(function (data) {
                $("#message").html('Fiche de frais mise en paiement').show();
            })
    });
</script>