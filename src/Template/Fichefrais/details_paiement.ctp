<div class="alert-success">Etat : <?=$statut_fiche_frais['Statuts']['libelle']?><br>Montant validé : <?=$montantValide_fiche_frais['Fichefrais']['montantValide']?> €</div>
<div id="id_fichefrais" style="display:none;"><?=$id_fichefrais?></div>
<div id="message"></div>
<table cellpadding="0" cellspacing="0">
    <legend><?= __('Eléments forfaitisés') ?></legend>
    <thead>
        <tr>
            <th><?= __('Frais étapes') ?></th>
            <th><?= __('Frais killométriques') ?></th>
            <th><?= __('Nuitées Hotêls') ?></th>
            <th><?= __('Repas Restaurants') ?></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>2</td>
            <td>50 km</td>
            <td>6 nuits</td>
            <td>5 repas</td>
        </tr>
    </tbody>
</table>
<table cellpadding="0" cellspacing="0">
    <legend><?= __('Descriptif des éléments hors forfait')?> - <?=$nb_justifs?> <?= __(' justificatifs reçus -') ?></legend>
    <thead>
        <tr>
            <th><?= __('Date') ?></th>
            <th><?= __('Libelle') ?></th>
            <th><?= __('Montant') ?></th>
            <th><?= __('Justificatif') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($fraishorsforfaits as $frais): ?>
          <tr>
              <td><?= date("d/m/Y", strtotime($frais->date)) ?></td>
              <td><?= h($frais->libelle) ?></td>
              <td><?= h($frais->montant) ?> €</td>
              <td>
                <?php if($frais['justificatifs']['lien_fichier'] != null): ?>
                  <a href="<?=$frais['justificatifs']['lien_fichier']?>" title="<?=$frais['justificatifs']['titre']?>"><span class="glyphicon glyphicon-file" aria-hidden="true"></span></a></td>
                <?php endif; ?>
              </td>
          </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<div id="changement_statut">
  <h4>Changer de statut</h4>
  <select name="statut" id="statut" class="form-control">
  	<option value="1">En cours de saisie</option>
    <option value="2">Validée</option>
    <option value="3">Mise en paiment</option>
  </select>
</div>