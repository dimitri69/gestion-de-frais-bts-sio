<?= $this->Html->css('bootstrap/bootstrap.min.css') ?>
<div class="fichefrais form large-10 medium-9 columns content">
  <nav class="" id="">
      <ul class="">
          <li><?= $this->Html->link(__('Fiche Forfait'), ['controller' => 'Fichefrais', 'action' => 'addForfait']) ?></li>
          <li><?= $this->Html->link(__('Fiche Hors Forfait'), ['controller' => 'Fichefrais', 'action' => 'addHorsForfait']) ?></li>
      </ul>
  </nav>


            <?= $this->Form->create($fraisforfaits) ?>
  <fieldset>
      <legend><?= __('Ajouter une fiche Forfait') ?></legend>
        <div class="alert-success">Prix de base Forfait : 0.67 €</div>
        <?php
            echo $this->Form->input('Fraisforfaits.id_Types', array('options' => $types_forfait));
            echo $this->Form->input('Ficheforfaits.0.quantite');
            echo $this->Form->input('Fraisforfaits.description');
            //echo $this->Form->input('Fraisforfaits.date_debut', array('value'=>'11/02/2016'));
            echo '<input type="text" name="date_debutt" value="10/24/1984" />';
            echo $this->Form->input('Fraisforfaits.date_fin');
            echo $this->Form->input('Fichefrais.mois');
            echo $this->Form->input('Fichefrais.id_utilisateur');
            echo $this->Form->input('Ficheforfaits.0.mois');
            echo $this->Form->input('Ficheforfaits.0.id_frais_forfait');
        ?>
  </fieldset>
    <div class="input-group date" data-provide="datepicker">
        <input type="text" class="form-control">
        <div class="input-group-addon">
            <span class="glyphicon glyphicon-th"></span>
        </div>
    </div>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
</div>
<script>
    $(function() {
        $('input[name="date_debut"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true
        });
    });
</script>