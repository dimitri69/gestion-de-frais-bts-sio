<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Fichefrai'), ['action' => 'edit', $fichefrai->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Fichefrai'), ['action' => 'delete', $fichefrai->id], ['confirm' => __('Are you sure you want to delete # {0}?', $fichefrai->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Fichefrais'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Fichefrai'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="fichefrais view large-9 medium-8 columns content">
    <h3><?= h($fichefrai->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($fichefrai->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Mois') ?></th>
            <td><?= $this->Number->format($fichefrai->mois) ?></td>
        </tr>
        <tr>
            <th><?= __('MontantValide') ?></th>
            <td><?= $this->Number->format($fichefrai->montantValide) ?></td>
        </tr>
        <tr>
            <th><?= __('Id User') ?></th>
            <td><?= $this->Number->format($fichefrai->id_user) ?></td>
        </tr>
        <tr>
            <th><?= __('Id Statu') ?></th>
            <td><?= $this->Number->format($fichefrai->id_statu) ?></td>
        </tr>
        <tr>
            <th><?= __('DateAjout') ?></th>
            <td><?= h($fichefrai->dateAjout) ?></td>
        </tr>
        <tr>
            <th><?= __('DateModif') ?></th>
            <td><?= h($fichefrai->dateModif) ?></td>
        </tr>
    </table>
</div>
