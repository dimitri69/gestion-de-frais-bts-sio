<div class="fichefrais form large-10 medium-9 columns content">
  <nav class="" id="">
      <ul class="">
          <li><?= $this->Html->link(__('Fiche Forfait'), ['controller' => 'Fichefrais', 'action' => 'addForfait']) ?></li>
          <li><?= $this->Html->link(__('Fiche Hors Forfait'), ['controller' => 'Fichefrais', 'action' => 'addHorsForfait']) ?></li>
      </ul>
  </nav>

  <?= $this->Form->create($fraihorsforfait) ?>
<fieldset>
<legend><?= __('Ajouter une fiche Hors Forfait') ?></legend>
<div class="alert-success">Prix de base Forfait : 0.67 €</div>
<?php
  echo $this->Form->input('Fraishorsforfaits.date');
  echo $this->Form->input('Fraishorsforfaits.libelle');
  echo $this->Form->input('Fraishorsforfaits.montant');
  echo $this->Form->input('Justificatifs.titre');
?>
</fieldset>
  <?= $this->Form->button(__('Submit')) ?>
  <?= $this->Form->end() ?>
</div>
