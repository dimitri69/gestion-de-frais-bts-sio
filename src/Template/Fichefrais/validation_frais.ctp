<style>
    .alphabet {
        width: 8px;
        padding: 10px 31px 10px 20px;
        background: #f0f0f0;
        display: inline-block;
    }
    #block-alphabet {
        text-align: center;
    }
    #resultat {
        margin-top: 50px;
    }
    tr:hover {
        cursor: pointer;
        background-color: grey !important;
        color: white;
    }
    .modal-header {
        padding: 15px 15px 0;
    }
    button {
        width: 70px;
    }
</style>

<div class="fichefrais view large-10 medium-9 columns content" style="background-color: #EE8855;">

    <?php
    if(!empty($list_date)){
        echo $this->Form->input('date_select', array('options' => $list_date, 'default'=>$mois));
    }
    ?>

    <div id="block-alphabet">
        <?php
        for ($lettre = 'A'; $lettre != 'AA'; $lettre++) {
            echo $this->Html->link($lettre, ['controller' => 'Fichefrais', 'action' => 'validationFrais', $mois, $lettre], ['class' => 'alphabet']);
        }
        ?>
    </div>
    <div id="resultat">
        <table cellpadding="0" cellspacing="0">
            <thead>
            <tr>
                <th><?= __('Nom') ?></th>
                <th><?= __('Prénom') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($list_utilisateurs as $utilisateur): ?>
                <a href="details_validation/<?= $utilisateur['id'] ?>/<?= $mois ?>">
                    <tr onclick="location.href='../../details_validation/<?= $utilisateur['id'] ?>/<?= $mois ?>'">
                        <td><?= $utilisateur['nom'] ?></td>
                        <td><?= $utilisateur['prenom'] ?></td>
                    </tr>
                </a>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
    $( "#date-select" ).change(function() {
        var date=$(this).val();
        var lettre = "<?=$lettre_choisie?>";
        location.href="../"+date+"/"+lettre;
    });
</script>
