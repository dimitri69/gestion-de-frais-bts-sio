<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Intranet du Laboratoire Galaxy-Swiss Bourdin - Comptable';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js'); ?>
    <?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.4.1/jsgrid.min.js'); ?>
    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <?= $this->Html->css('//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css') ?>
    <?= $this->Html->css('//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css') ?>
    <!--<?//= $this->Html->script('http://eternicode.github.io/bootstrap-datepicker/bootstrap-datepicker/js/bootstrap-datepicker.js') ?>
    <?//= $this->Html->script('http://eternicode.github.io/bootstrap-datepicker/bootstrap-datepicker/js/locales/bootstrap-datepicker.fr.js') ?>-->

    <?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.4.1/jsgrid.min.js'); ?>
    <?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.4.1/jsgrid.min.css') ?>
    <?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.4.1/jsgrid-theme.min.css') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>
    <?= $this->Html->css('style.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
  <div id="page">
    <div id="entete">
      <?= $this->Html->image('logo_tr.png', ['id'=>"logoGSB", 'alt' => 'Laboratoire Galaxy-Swiss Bourdin']); ?>
      <h1><?= __('Suivi du remboursement des frais')?></h1>
    </div>
    <nav class="top-bar expanded" data-topbar role="navigation">
        <ul class="title-area large-2 medium-3 columns">
            <li class="name">
                <h1><a href=""><?= $this->fetch('title') ?></a></h1>
            </li>
        </ul>
        <section class="top-bar-section">
            <span id="titre_site"><?= $this->request->session()->read('Auth.User.prenom')." ".$this->request->session()->read('Auth.User.nom'); ?></span>
            <ul class="right">
                <li><?= $this->Html->link(__('Déconnexion'), ['controller' => 'Users', 'action' => 'logout']) ?></li>
            </ul>
        </section>
    </nav>
    <?= $this->Flash->render() ?>
    <section class="container clearfix">
      <nav class="large-2 medium-3 columns" id="actions-sidebar">
          <ul class="side-nav">
              <li class="heading"><?= __('Menu') ?></li>
              <li><?= $this->Html->link(__('Suivi des paiements'), ['controller' => 'Fichefrais', 'action' => 'suiviPaiement']) ?></li>
              <li><?= $this->Html->link(__('Validation des fiche de frais'), ['controller' => 'Fichefrais', 'action' => 'validationFrais']) ?></li>
          </ul>
      </nav>
        <?= $this->fetch('content') ?>
    </section>
    <footer>
    </footer>
  </div>
</body>
</html>
