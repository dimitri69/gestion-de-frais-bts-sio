<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Intranet du Laboratoire Galaxy-Swiss Bourdin - Comptable';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('bootstrap/bootstrap.min.css') ?>

    <?= $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js'); ?>
    <?= $this->Html->css('//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css') ?>
    <?= $this->Html->script('bootstrap/bootstrap.min.js') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>
    <?= $this->Html->css('style.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
<div id="page">
    <div id="entete">
        <?= $this->Html->image('logo_tr.png', ['id'=>"logoGSB", 'alt' => 'Laboratoire Galaxy-Swiss Bourdin']); ?>
        <h1><?= __('Suivi du remboursement des frais')?></h1>
    </div>
    <nav class="top-bar expanded" data-topbar role="navigation">
        <ul class="title-area large-2 medium-3 columns">
            <li class="name">
                <h1><a href=""><?= $this->fetch('title') ?></a></h1>
            </li>
        </ul>
        <section class="top-bar-section">
            <span id="titre_site"><?= $this->request->session()->read('Auth.User.prenom')." ".$this->request->session()->read('Auth.User.nom'); ?></span>
            <ul class="right">
                <li><?= $this->Html->link(__('Déconnexion'), ['controller' => 'Users', 'action' => 'logout']) ?></li>
            </ul>
        </section>
    </nav>
    <?= $this->Flash->render() ?>
    <section class="container clearfix">
        <div class="fichefrais form large-12 medium-10 columns content">
            <?= $this->Flash->render('auth') ?>
            <h3>Problème d'accès à cette page.</h3><br>
            <h4>Vous n'êtes pas autoriser à accéder à cette page, compte tenu de vos droits.</h4><br><br>
            <a href="" onClick="javascript:window.history.go(-1)">Revenir à la page précédente</a><br><br><br><br><br>
        </div>
    </section>
    <footer>
    </footer>
</div>
</body>
</html>
