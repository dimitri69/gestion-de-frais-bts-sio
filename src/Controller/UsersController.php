<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Utilisateurs Controller
 *
 * @property \App\Model\Table\UtilisateursTable $Utilisateurs
 */
class UsersController extends AppController
{

    /**
     * Index method
     * @return void
     */
    public function index()
    {
        $this->viewBuilder()->layout('admin');
        $this->set('users', $this->paginate($this->Users));
        $this->set('_serialize', ['users']);
    }

    public function login()
    {
      if ($this->request->is('post')) {
          $user = $this->Auth->identify();
          if ($user) {
              $this->Auth->setUser($user);
              $user_profile = $user['id_profile'];
              debug($user_profile);
              if($user_profile == 1){   // Admin
                  return $this->redirect(['controller' => 'Users', 'action' => 'index']);
              }
              else if($user_profile == 2){  // Visiteur
                  return $this->redirect(['controller' => 'Fichefrais', 'action' => 'index']);
              }
              else if($user_profile == 3){   // Comptable
                  return $this->redirect(['controller' => 'Fichefrais', 'action' => 'validation-frais']);
              }
              else{
                  return $this->redirect($this->Auth->redirectUrl());
              }
          } else {
              $this->Flash->error(__('Nom d\'utilisateur ou mot de passe incorrect'), [
                  'key' => 'auth'
              ]);
          }
      }
      $this->viewBuilder()->layout('login');
    }

    public function logout()
    {
        $this->Flash->success('Vous êtes maintenant déconnecté.');
        return $this->redirect($this->Auth->logout());
    }

    public function errorUnauthorized(){
        $this->viewBuilder()->layout('error_unauthorized');
    }


    /**
     * View method
     * @param string|null $id User id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->viewBuilder()->layout('admin');
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->viewBuilder()->layout('admin');
        $users = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $users = $this->Users->patchEntity($users, $this->request->data);
            if ($this->Users->save($users)) {
                $this->Flash->success(__('L\'utilisateur a bien été enregistré.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('L\'utilisateur n\'a pas été enregistré. Veuillez réessayer.'));
            }
        }
        $this->loadModel("Profiles");
        $profils = $this->Users->Profiles->find('list', ['fiedls' => array('Profiles.id', 'Profiles.libelle'), 'keyField' => 'id', 'valueField' => 'libelle']);
        $this->set('profils', $profils);
        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * Edit method
     * @param string|null $id User id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->viewBuilder()->layout('admin');
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('L\'utilisateur a bien été enregistré.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('L\'utilisateur n\'a pas été enregistré. Veuillez réessayer.'));
            }
        }
        $profils = $this->Users->Profiles->find('list', ['fiedls' => array('Profiles.id', 'Profiles.libelle'), 'keyField' => 'id', 'valueField' => 'libelle']);
        $this->set('profils', $profils);
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('L\'utilisateur a bien été supprimé.'));
        } else {
            $this->Flash->error(__('L\'utilisateur n\'a pas pu être supprimé. Veuillez réessayer.'));
        }
        return $this->redirect(['action' => 'index']);
    }


    public function beforeFilter(Event $event)
    {
       parent::beforeFilter($event);
       if(!empty($this->request->session()->read('Auth.User'))){
         $user_profile = $this->request->session()->read('Auth.User.id_profile');

         if($user_profile == 1){  // Administrateur
            return true;
          }
          else{
              if($this->request->action == 'login' || $this->request->action == 'logout' || $this->request->action == 'errorUnauthorized'){
                return true;
              }
              else{
                return $this->redirect(['controller' => 'Users', 'action' => 'errorUnauthorized']);
              }
          }
      }
    }

}
