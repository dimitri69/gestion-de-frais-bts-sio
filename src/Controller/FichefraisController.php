<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
 * Fichefrais Controller
 *
 * @property \App\Model\Table\FichefraisTable $Fichefrais
 */
class FichefraisController extends AppController
{

    public $components = array(
        'UserPermissions.UserPermissions'
    );

    /**
     *  Consulter mes fiches de frais (visiteur)
     */
    public function index($mois = null)
    {
        // Initiatlisation de la variable user_id par l'utilisateur connecté en Session & Initialisation de la date.
        $user_id = $this->request->session()->read('Auth.User.id');
        if ($mois == null) {
            $mois = date("Yn");
        }

        if ($this->request->is('post'))  // si formulaire soumis
        {
            $mois = $this->request->data['date'];
            return $this->redirect(['controller' => 'Fichefrais', 'action' => 'index', $mois]);
        }

        // Récupération des frais hors-forfait pour le mois en cours et pour l'utilisateur connecté en session
        $this->loadModel('Fraishorsforfaits');
        $this->set('fraishorsforfaits', $this->Fraishorsforfaits->find("all", [
            'fields' => array('Fraishorsforfaits.date', 'Fraishorsforfaits.montant', 'Fraishorsforfaits.libelle', 'justificatifs.titre', 'justificatifs.lien_fichier'),
            'conditions' => array('Fichefrais.id_user' => $user_id, 'Fichefrais.mois' => $mois),
            'contain' => array('Fichefrais'),
            'join' => array(array(
                'table' => 'justificatifs', 'alias' => 'justificatifs', 'type' => 'LEFT',
                'conditions' => array('justificatifs.id_fraishorsforfaits = Fraishorsforfaits.id')
            ))
        ]));

        // Récupération des frais forfaitisés pour le mois en cours et pour l'utilisateur connecté en session
        $this->loadModel('Ficheforfaits');
        $fiches_frais = $this->Ficheforfaits->find("all", [
            'conditions' => array('Fichefrais.id_user' => $user_id, 'Fichefrais.mois' => $mois),
            'contain' => array('Fichefrais', 'Fraisforfaits')
        ]);

        $donnees_forfaits = array('etape' => array('qte' => 0), 'hotel' => array('qte' => 0), 'km' => array('qte' => 0), 'repas' => array('qte' => 0));
        // Initialisation du tableau des donnes des somems de chaque type de frais forfaitisé
        foreach ($fiches_frais->toArray() as $key => $value) {
            if ($value['fraisforfait']['id_types'] == 1) {
                $donnees_forfaits['etape']['qte'] = $donnees_forfaits['etape']['qte'] + $value['quantite'];
            }
            if ($value['fraisforfait']['id_types'] == 2) {
                $donnees_forfaits['hotel']['qte'] = $donnees_forfaits['hotel']['qte'] + $value['quantite'];
            }
            if ($value['fraisforfait']['id_types'] == 3) {
                $donnees_forfaits['km']['qte'] = $donnees_forfaits['km']['qte'] + $value['quantite'];
            }
            if ($value['fraisforfait']['id_types'] == 4) {
                $donnees_forfaits['repas']['qte'] = $donnees_forfaits['km']['qte'] + $value['quantite'];
            }
        }
        $this->set('donnees_forfaits', $donnees_forfaits);

        // Récupération des mois où une fiche de frais existe pour l'utilisateur conencté, pour remplir le SelectInput
        $this->loadModel('Fichefrais');
        $list_date = $this->Fichefrais->getLesMoisDisponibles($user_id);
        $this->set(compact('list_date'));
        if (!empty($list_date)) {
            $lesCles = array_keys($list_date);
            $moisASelectionner = $lesCles[0];
            $this->set(compact('mois'));
        }

        // Récupération du statut et du montant validé de la fiche de frais en cours
        $this->set('statut_fiche_frais', $this->Fichefrais->getStatutFichefrais($user_id, $mois));
        $this->set('montantValide_fiche_frais', $this->Fichefrais->getMontantValide($user_id, $mois));

    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     *  Valider fiche de frais (comptable)
     */
    public function validationFrais($mois = null, $lettre_choisie = null)
    {
        // Appel au Layout pour les comptable (menu qui change)
        $this->viewBuilder()->layout('comptable');

        if ($mois == null) {
            $mois = date("Yn");
        }
        $this->set(compact('mois'));

        if ($lettre_choisie == null) {
            $lettre_choisie = "A";
        }
        $this->set(compact('lettre_choisie'));

        // Récupération des utilisateurs dont le nom commence par la lettre sélectionnée
        $this->loadModel('Users');
        $list_utilisateurs = $this->Users->find("all", ['fields' => array('id', 'nom', 'prenom'), 'conditions' => array('nom LIKE' => "$lettre_choisie%", 'id_profile' => 2)]);
        $this->set(compact('list_utilisateurs'));

        // Récupération des mois où une fiche de frais existe pour l'utilisateur conencté, pour remplir le SelectInput
        $this->loadModel('Fichefrais');
        $list_date = $this->Fichefrais->getLesMoisDisponibles();
        $this->set(compact('list_date'));
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     *  Chargement Ajax du modal lors du clic sur un utilisateur, sur la page fichefrais/validation-frais
     */
    public function detailsValidation($user_id, $mois)
    {
        // Appel au Layout pour les comptable (menu qui change)
        $this->viewBuilder()->layout('comptable');

        // Récupération des frais hors-forfait pour le mois en cours et pour l'utilisateur connecté en session
        $this->loadModel('Fraishorsforfaits');
        $this->set('fraishorsforfaits', $this->Fraishorsforfaits->find("all", ['fields' => array('Fraishorsforfaits.id', 'Fraishorsforfaits.date', 'Fraishorsforfaits.montant', 'Fraishorsforfaits.libelle', 'justificatifs.titre', 'justificatifs.lien_fichier'), 'conditions' => array('Fichefrais.id_user' => $user_id, 'Fichefrais.mois' => $mois), 'contain' => array('Fichefrais'), 'join' => array(array('table' => 'justificatifs',
            'alias' => 'justificatifs',
            'type' => 'LEFT',
            'conditions' => array('justificatifs.id_fraishorsforfaits = Fraishorsforfaits.id')))]));

        $this->set('nb_justifs', $this->Fraishorsforfaits->getNbJustifs($user_id, $mois));

        // Récupération des frais forfaitisés pour le mois en cours et pour l'utilisateur connecté en session
        $this->loadModel('Ficheforfaits');
        $fichesforfaits = $this->Ficheforfaits->find("all", ['fields' => array('fraisforfaits.id', 'Ficheforfaits.quantite', 'fraisforfaits.id_types', 'fraisforfaits.description', 'fraisforfaits.date_debut', 'fraisforfaits.date_fin', 'types.libelle', 'types.montant'), 'conditions' => array('Fichefrais.id_user' => $user_id, 'Fichefrais.mois' => $mois), 'join' => array(
            array('table' => 'fichefrais',
                'alias' => 'fichefrais',
                'type' => 'LEFT',
                'conditions' => array('fichefrais.id = Ficheforfaits.id_fichefrais')
            ),
            array('table' => 'fraisforfaits',
                'alias' => 'fraisforfaits',
                'type' => 'LEFT',
                'conditions' => array('fraisforfaits.id = Ficheforfaits.id_fraisforfaits')
            ),
            array('table' => 'types',
                'alias' => 'types',
                'type' => 'LEFT',
                'conditions' => array('types.id = Fraisforfaits.id_types')
            )
        )
        ]);
        $this->set('fichesforfaits', $fichesforfaits);

        // Récupération du statut et du montant validé de la fiche de frais en cours
        $this->loadModel('Fichefrais');
        $this->set('statut_fiche_frais', $this->Fichefrais->getStatutFichefrais($user_id, $mois));
        $this->set('montantValide_fiche_frais', $this->Fichefrais->getMontantValide($user_id, $mois));
        $this->set('id_fichefrais', $this->Fichefrais->getId($user_id, $mois));
        $this->set('mois', $mois);
        $this->set('user_id', $user_id);
        $this->loadModel('Statuts');
        $this->set('list_statuts', $this->Statuts->getStatutsFraisHorsForfaits());

        // On renvoie la vue du contenu au modal bootstrap
        $this->render('details_validation');

    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     *  Sauvegarde des frais hors-forfaits du modal de validation de la fiche de frais
     */
    public function ajaxSaveValidation()
    {
        $user_id = $this->request->session()->read('Auth.User.id');
        $mois = date('Ym', strtotime('+1 months'));

        if (!empty($this->request->data['frais']) && $this->request->data['frais'] == 'hors_forfait') {
            if (!empty($this->request->data['choix']) && !empty($this->request->data['id_horsforfait'])) {

                // Mise à jour des frais hors forfaits
                $FraishorsforfaitsTable = TableRegistry::get('Fraishorsforfaits');
                $fraishorsforfait = $FraishorsforfaitsTable->newEntity();

                // Récupération des infos du frais hors forfait refusé
                $this->loadModel('Fraishorsforfaits');
                $infos_frais_reporte = $this->Fraishorsforfaits->find("all", ['conditions' => array('Fraishorsforfaits.id' => $this->request->data['id_horsforfait'])])->toArray();

                // Si le choix du frais hors-forfait est refusée, alors on met à jour son libellé en rajoutant le mot "REFUSE"
                if ($this->request->data['choix'] == 'refuser') {
                    $fraishorsforfait->libelle = "REFUSE - " . $infos_frais_reporte[0]['libelle'];
                    $fraishorsforfait->id = $infos_frais_reporte[0]['id'];
                } // Si le choix du frais hors-forfait est reposrtée, alors on ajoute les infos du frais hors-forfaits dans la fiche de frais du prochaine mois. Et  on le suprrime du mois en cours
                elseif ($this->request->data['choix'] == 'reporter') {

                    // Récupération des infos du frais hors forfait reporté
                    $this->loadModel('Fraishorsforfaits');
                    $infos_frais_reporte = $this->Fraishorsforfaits->find("all", ['conditions' => array('Fraishorsforfaits.id' => $this->request->data['id_horsforfait'])])->toArray();

                    // Récupération de la fiche de frais de l'user_id du mois prochaine. Si elle n'existe pas encore, on la crée.
                    $fiche_frais_exist = $this->Fichefrais->find("all", ['conditions' => array('id_user' => $user_id, 'mois' => $mois)]);
                    $fiche_frais_exist = $fiche_frais_exist->first();

                    if (empty($fiche_frais_exist)) {
                        $fiche_frais = TableRegistry::get('Fichefrais');
                        $date_now = date("Y-m-d");
                        $data = array('id_user' => (int)$user_id, 'mois' => (int)$mois, 'montantValide' => 0, 'dateAjout' => $date_now, 'dateModif' => $date_now, 'id_statut' => 1);
                        $entity = $fiche_frais->newEntity($data, ['validate' => true]);
                        $save_newfiche = $this->Fichefrais->save($entity);

                        if ($save_newfiche) {
                            $this->Flash->success(__('La nouvelle fiche de frais du mois ' . $mois . " a été créée."));
                        } else {
                            $this->Flash->error(__('La nouvelle fiche de frais n\'a pas pu être créée'));
                        }
                    }
                    // Enregistrement du frais hors forfait dans la fiche de frais du prochain mois
                    $fraishorsforfait->date = $infos_frais_reporte->date;
                    $fraishorsforfait->montant = $infos_frais_reporte->montant;
                    $fraishorsforfait->libelle = $infos_frais_reporte->libelle;
                    $fraishorsforfait->nb_justificatif = 1;
                    $fraishorsforfait->id_fichefrais = $save_newfiche->id;
                    $save_fraishorsforfait = $FraishorsforfaitsTable->save($fraishorsforfait);
                }
            }

            // Validation de la fiche de frais
            $Tablefiche_frais = TableRegistry::get('Fichefrais');
            $fichefrais = $Tablefiche_frais->newEntity();
            $fichefrais->id_statut = 2;
            $fichefrais->montantValide = floatval($this->request->data['montantValideForfait']) + floatval($this->request->data['montantValideHorsForfait']);
            $fichefrais->id = $this->request->data['id_fichefrais'];
            $Tablefiche_frais->save($fichefrais);
        }

        return $this->redirect(['controller' => 'Fichefrais', 'action' => 'ajaxdetailsValidation', $user_id, $mois]);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     *  Suivre le paiement fiche de frais
     */
    public function suiviPaiement()
    {
        // Appel au Layout pour les comptable (menu qui change)
        $this->viewBuilder()->layout('comptable');

        $this->loadModel('Fichefrais');
        $this->set('fichefraisvalides', $this->Fichefrais->find("all", ['conditions' => array('Fichefrais.id_statut' => 2), 'contain' => array('Users', 'Statuts')]));
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     *  Chargement Ajax du modal lors du clic sur une fiche de frais, sur la page fichefrais/suivi-paiement
     */
    public function ajaxdetailsPaiment($id_fichefrais)
    {
        $this->loadModel('Fraishorsforfaits');
        $this->set('nb_justifs', $this->Fraishorsforfaits->getNbJustifsById($id_fichefrais));

        // Récupération des frais hors-forfait pour le mois en cours et pour l'utilisateur connecté en session
        $this->loadModel('Fraishorsforfaits');
        $this->set('fraishorsforfaits', $this->Fraishorsforfaits->find("all", ['fields' => array('Fraishorsforfaits.id', 'Fraishorsforfaits.date', 'Fraishorsforfaits.montant', 'Fraishorsforfaits.libelle', 'justificatifs.titre', 'justificatifs.lien_fichier'), 'conditions' => array('Fichefrais.id' => $id_fichefrais), 'contain' => array('Fichefrais'), 'join' => array(array('table' => 'justificatifs',
            'alias' => 'justificatifs',
            'type' => 'LEFT',
            'conditions' => array('justificatifs.id_fraishorsforfaits = Fraishorsforfaits.id')))]));

        // Récupération des frais forfaitisés pour le mois en cours et pour l'utilisateur connecté en session
        $this->loadModel('Ficheforfaits');
        $fiches_frais = $this->Ficheforfaits->find("all", ['conditions' => array('Fichefrais.id' => $id_fichefrais), 'contain' => array('Fichefrais', 'Fraisforfaits')]);

        $donnees_forfaits = array('etape' => array('qte' => 0), 'hotel' => array('qte' => 0), 'km' => array('qte' => 0), 'repas' => array('qte' => 0)); // Initialisation du tableau des donnes des somems de chaque type de frais forfaitisé
        foreach ($fiches_frais->toArray() as $key => $value) {
            if ($value['fraisforfait']['id_types'] == 1) {
                $donnees_forfaits['etape']['qte'] = $donnees_forfaits['etape']['qte'] + $value['quantite'];
            }
            if ($value['fraisforfait']['id_types'] == 2) {
                $donnees_forfaits['hotel']['qte'] = $donnees_forfaits['hotel']['qte'] + $value['quantite'];
            }
            if ($value['fraisforfait']['id_types'] == 3) {
                $donnees_forfaits['km']['qte'] = $donnees_forfaits['km']['qte'] + $value['quantite'];
            }
            if ($value['fraisforfait']['id_types'] == 4) {
                $donnees_forfaits['repas']['qte'] = $donnees_forfaits['km']['qte'] + $value['quantite'];
            }
        }
        $this->set('donnees_forfaits', $donnees_forfaits);

        // Récupération du statut et du montant validé de la fiche de frais en cours
        $this->loadModel('Fichefrais');
        $this->set('statut_fiche_frais', $this->Fichefrais->getStatutFichefraisById($id_fichefrais));
        $this->set('montantValide_fiche_frais', $this->Fichefrais->getMontantValideById($id_fichefrais));
        $this->set('id_fichefrais', $id_fichefrais);

        // On renvoie la vue du contenu au modal bootstrap
        $this->render('details_paiement');
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     *  Chargement Ajax du modal lors du clic sur une fiche de frais, sur la page fichefrais/suivi-paiement
     */
    public function ajaxMisePaiement()
    {
        if (!empty($this->request->data['choix_statut']) && !empty($this->request->data['id_fichefrais'])) {
            $Tablefiche_frais = TableRegistry::get('Fichefrais');
            $fichefrais = $Tablefiche_frais->newEntity();
            $fichefrais->id_statut = $this->request->data['choix_statut'];
            $fichefrais->id = $this->request->data['id_fichefrais'];
            $Tablefiche_frais->save($fichefrais);
        }
        return $this->redirect(['controller' => 'Fichefrais', 'action' => 'suiviPaiement']);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Saisir fiche de frais
     */
    public function add()
    {
        // Initiatlisation de la variable user_id par l'utilisateur connecté en Session & Initialisation du mois en cours
        $user_id = $this->request->session()->read('Auth.User.id');
        $mois = date("Yn");
        $date_now = date("Y-m-d");

        // Vérification si la fiche de frais du mois existe pour l'utilisateur connecté, si NON, alors on la crée
        $fiche_frais_exist = $this->Fichefrais->find("all", ['conditions' => array('id_user' => $user_id, 'mois' => $mois)]);
        $fiche_frais_exist = $fiche_frais_exist->first();

        if (empty($fiche_frais_exist)) {
            $fiche_frais = TableRegistry::get('Fichefrais');
            $data = array('id_user' => (int)$user_id, 'mois' => (int)$mois, 'montantValide' => 0, 'dateAjout' => $date_now, 'dateModif' => $date_now, 'id_statut' => 1);
            $entity = $fiche_frais->newEntity($data, ['validate' => true]);
            if ($this->Fichefrais->save($entity)) {
                $this->Flash->success(__('La nouvelle fiche de frais du mois ' . $mois . " a été créée."));
            } else {
                $this->Flash->error(__('La nouvelle fiche de frais n\'a pas pu être créée'));
            }
        }

        // Récupération des frais hors-forfait pour le mois en cours et pour l'utilisateur connecté en session
        $this->loadModel('Fraishorsforfaits');
        $this->set('fraishorsforfaits', $this->Fraishorsforfaits->find("all", [
            'fields' => array('Fraishorsforfaits.date', 'Fraishorsforfaits.montant', 'Fraishorsforfaits.libelle', 'justificatifs.titre', 'justificatifs.lien_fichier'),
            'conditions' => array('Fichefrais.id_user' => $user_id, 'Fichefrais.mois' => $mois),
            'contain' => array('Fichefrais'),
            'join' => array(
                array(
                    'table' => 'justificatifs',
                    'alias' => 'justificatifs',
                    'type' => 'LEFT',
                    'conditions' => array('justificatifs.id_fraishorsforfaits = Fraishorsforfaits.id')
                )
            )
        ]));

        // Récupération des frais forfaitisés pour le mois en cours et pour l'utilisateur connecté en session
        $this->loadModel('Ficheforfaits');
        $fichesforfaits = $this->Ficheforfaits->find("all", [
            'fields' => array('Ficheforfaits.quantite', 'fraisforfaits.description', 'fraisforfaits.date_debut', 'fraisforfaits.date_fin', 'types.libelle', 'types.montant'),
            'conditions' => array('fichefrais.id_user' => $user_id, 'fichefrais.mois' => $mois),
            'join' => array(
                array('table' => 'fichefrais',
                    'alias' => 'fichefrais',
                    'type' => 'LEFT',
                    'conditions' => array('fichefrais.id = Ficheforfaits.id_fichefrais')
                ),
                array('table' => 'fraisforfaits',
                    'alias' => 'fraisforfaits',
                    'type' => 'LEFT',
                    'conditions' => array('fraisforfaits.id = Ficheforfaits.id_fraisforfaits')
                ),
                array('table' => 'types',
                    'alias' => 'types',
                    'type' => 'LEFT',
                    'conditions' => array('types.id = fraisforfaits.id_types')
                )
            )
        ]);
        $this->set('fichesforfaits', $fichesforfaits);

        // AJOUT DES FRAIS HORS FORFAITS
        if ($this->request->is('post'))  // si le formulaire d'ajout de frais hors forfait est soumis
        {
            $libelle_horsforfait = $this->request->data['libelle_horsforfait'];
            $date_horsforfait = $this->request->data['date_horsforfait'];
            $montant_horsforfait = $this->request->data['montant_horsforfait'];
            $justif_horsforfait = $this->request->data['Justificatif_horsforfait'];
            $titre_horsforfait = $this->request->data['titre_justif_horsforfait'];

            // Récupération de l'id de la fiche de frais en cours
            $this->loadModel('Fichefrais');
            $id_fichefrais = $this->Fichefrais->find("all", ['fields' => 'id', 'conditions' => array('Fichefrais.id_user' => $user_id, 'Fichefrais.mois' => $mois)])->first();

            // Sauvegarde de la ligne hors forfait
            $FraishorsforfaitsTable = TableRegistry::get('Fraishorsforfaits');
            $fraishorsforfait = $FraishorsforfaitsTable->newEntity();
            $fraishorsforfait->id_fichefrais = $id_fichefrais->id;
            $fraishorsforfait->date = $date_horsforfait;
            $fraishorsforfait->montant = $montant_horsforfait;
            $fraishorsforfait->libelle = $libelle_horsforfait;
            if (is_uploaded_file($_FILES['Justificatif_horsforfait']['tmp_name'])) {
                $fraishorsforfait->nb_justificatif = 1;
            } else {
                $fraishorsforfait->nb_justificatif = 0;
            }
            $save_fraishorsforfait = $FraishorsforfaitsTable->save($fraishorsforfait);


            $content_dir = WWW_ROOT . 'upload' . DS; // dossier où sera déplacé le fichier
            $tmp_file = $_FILES['Justificatif_horsforfait']['tmp_name'];

            if (!is_uploaded_file($tmp_file)) {
                exit("Le fichier est introuvable");
            }
            // on vérifie maintenant l'extension
            $type_file = $_FILES['Justificatif_horsforfait']['type'];
            if (!strstr($type_file, 'pdf') && !strstr($type_file, 'doc') && !strstr($type_file, 'docx') && !strstr($type_file, 'application/msword')) {
                exit("Le fichier n'est pas une image");
            }
            if (strstr($type_file, 'application/pdf')) { 
                $format_fichier = 'pdf';
            } elseif (strstr($type_file, 'application/msword')) {
                $format_fichier = 'doc';
            } elseif (strstr($type_file, 'application/vnd.openxmlformats-officedocument.wordprocessingml.document')) {
                $format_fichier = 'docx';
            }

            // Sauvegarde du justificatif dans la table Justificatifs
            $JustificatifsTable = TableRegistry::get('Justificatifs');
            $justificatif = $JustificatifsTable->newEntity();
            $justificatif->lien_fichier = $content_dir . $user_id . '_' . $save_fraishorsforfait->id . '.' . $format_fichier;
            $justificatif->titre = $titre_horsforfait;
            $justificatif->type_fichier = $format_fichier;
            $justificatif->id_fraishorsforfaits = $save_fraishorsforfait->id;
            $save_justif = $JustificatifsTable->save($justificatif);

            // on copie le fichier dans le dossier de destination
            $name_file = $user_id . '_' . $save_fraishorsforfait->id . '.' . $format_fichier;
            if (!move_uploaded_file($tmp_file, $content_dir . $name_file)) {
                exit("Impossible de copier le fichier dans $content_dir");
            }
            //echo "Le fichier a bien été uploadé";
        }

        // Select des types de forfaits
        $this->loadModel("Types");
        $types = $this->Types->find('list', ['fiedls' => array('Types.id', 'Types.libelle'), 'keyField' => 'id', 'valueField' => 'libelle']);

        $this->set('types', $types);

    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Accès aux pages en fonction du profil de l'utilisateur
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $groups = array(
            1 => array('*'),
            2 => array('index', 'add', 'addForfait','jsGrid'),
            3 => array('validationFrais', 'suiviPaiement', 'ajaxdetailsValidation', 'ajaxdetailsPaiment', 'ajaxSaveValidation', 'ajaxMisePaiement', 'jsGrid', 'pageTest','detailsValidation')
        );

        $action = $this->request->params['action'];
        $controller = $this->request->params['controller'];
        $user_profile = $this->request->session()->read('Auth.User.id_profile');
        foreach ($groups as $group => $autor) {
            while ($group == $user_profile) {
                foreach ($autor as $action_group) {
                    $ok = true;
                    if ($action_group == $action) {
                        $ok = true;
                        break;
                    } else {
                        $ok = false;
                    }
                }
                if ($ok) {
                    break;
                } else {
                    return $this->redirect(['controller' => 'Users', 'action' => 'errorUnauthorized']);
                }
            }
        }
    }


    /**
     * Add method
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function addForfait()
    {
        // Initiatlisation de la variable user_id par l'utilisateur connecté en Session & Initialisation du mois en cours
        $user_id = $this->request->session()->read('Auth.User.id');
        $mois = date("Yn");

        if ($this->request->is('post'))  // si le formulaire d'ajout de frais hors forfait est soumis
        {
            $data = $this->request->data;

            $this->loadComponent('Fonctions');

            $FraisforfaitsTable = TableRegistry::get('Fraisforfaits');
            $fraisforfait = $FraisforfaitsTable->newEntity();
            $fraisforfait->description = $data['description_forfait'];
            $fraisforfait->date_debut = $this->Fonctions->dateFrancaisVersAnglais($data['date_debut_forfait']);
            $fraisforfait->date_fin = $this->Fonctions->dateFrancaisVersAnglais($data['date_fin_forfait']);
            $fraisforfait->id_types = $data['type_forfait'];
            $save_fraisfrofaits = $FraisforfaitsTable->save($fraisforfait);

            $this->loadModel('Fichefrais');
            $id_fichefrais = $this->Fichefrais->find("all", [
                'fields' => array('id'),
                'conditions' => array(
                    'Fichefrais.id_user' => $user_id,
                    'Fichefrais.mois' => $mois)
            ])->first();

            $FicheforfaitsTable = TableRegistry::get('Ficheforfaits');
            $ficheforfait = $FicheforfaitsTable->newEntity();
            $ficheforfait->id_fichefrais = $id_fichefrais->id;
            $ficheforfait->id_fraisforfaits = $save_fraisfrofaits->id;
            $ficheforfait->quantite = $data['qte_forfait'];

            if ($save_fraisfrofaits && $FicheforfaitsTable->save($ficheforfait)) {
                $this->Flash->success(__('Le frais forfaitisé a été ajouté'));
            }

            // Récupération des frais hors-forfait pour le mois en cours et pour l'utilisateur connecté en session
            $this->loadModel('Fraishorsforfaits');
            $this->set('fraishorsforfaits', $this->Fraishorsforfaits->find("all", [
                'conditions' => array('Fichefrais.id_user' => $user_id, 'Fichefrais.mois' => $mois),
                'contain' => 'Fichefrais'
            ]));

            // Récupération des frais forfaitisés pour le mois en cours et pour l'utilisateur connecté en session
            $this->loadModel('Ficheforfaits');
            $fichesforfaits = $this->Ficheforfaits->find("all", [
                'fields' => array('Ficheforfaits.quantite', 'fraisforfaits.description', 'fraisforfaits.date_debut', 'fraisforfaits.date_fin', 'types.libelle', 'types.montant'),
                'conditions' => array('fichefrais.id_user' => $user_id, 'fichefrais.mois' => $mois),
                'join' => array(
                    array('table' => 'fichefrais',
                        'alias' => 'fichefrais',
                        'type' => 'LEFT',
                        'conditions' => array('fichefrais.id = Ficheforfaits.id_fichefrais')
                    ),
                    array('table' => 'fraisforfaits',
                        'alias' => 'fraisforfaits',
                        'type' => 'LEFT',
                        'conditions' => array('fraisforfaits.id = Ficheforfaits.id_fraisforfaits')
                    ),
                    array('table' => 'types',
                        'alias' => 'types',
                        'type' => 'LEFT',
                        'conditions' => array('types.id = fraisforfaits.id_types')
                    )
                )
            ]);
            $this->set('fichesforfaits', $fichesforfaits);

            $this->render('elements/table_forfaits');
        }
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function addHorsForfait()
    {
        $this->loadModel("Fraishorsforfaits");
        $fraihorsforfait = $this->Fraishorsforfaits->newEntity();
        if ($this->request->is('post')) {
            $fraihorsforfait = $this->Fraishorsforfaits->patchEntity($fraihorsforfait, $this->request->data);
            $id_user = $this->request->session()->read('Auth.User.id');
            $mois = 201511;
            $fiche_frais_exist = $this->Fichefrais->find("all", ['conditions' => array('id_user' => $id_user, 'mois' => $mois)]);
            $fiche_frais_exist = $fiche_frais_exist->first();
            $fraihorsforfait['id_fichefrais'] = $fiche_frais_exist['id'];
            if ($this->Fraishorsforfaits->save($fraihorsforfait)) {
                $this->Flash->success(__('The fichefrai has been saved.'));
                //return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The fichefrai could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('fraihorsforfait'));
        $this->set('_serialize', ['fraihorsforfait']);
    }

    public function pageTest(){

    }

    public function jsGrid($user_id = null, $mois = null){

        if ($this->request->is('put')) {

            $data = $this->request->data;

            $FraisforfaitsTable = TableRegistry::get('Fraisforfaits');
            $fraisforfait = $FraisforfaitsTable->newEntity();
            $fraisforfait->id = $data['ID'];
            $fraisforfait->description = $data['Description'];
            $fraisforfait->date_debut = $data['Debut'];
            $fraisforfait->date_fin = $data['Fin'];
            $fraisforfait->id_types = $data['Type'];
            $save_fraisforfaits = $FraisforfaitsTable->save($fraisforfait);

            $this->loadModel('Fichefrais');
            $id_fichefrais = $this->Fichefrais->find("all", [
                'fields' => array('id'),
                'conditions' => array(
                    'Fichefrais.id_user' => $user_id,
                    'Fichefrais.mois' => $mois
                )
            ])->first();

            $FicheforfaitsTable = TableRegistry::get('Ficheforfaits');
            $ficheforfait = $FicheforfaitsTable->newEntity();
            $ficheforfait->quantite = $data['Quantite'];
            $ficheforfait->id_fichefrais = $id_fichefrais->id;
            $ficheforfait->id_fraisforfaits = $save_fraisforfaits->id;
            $FicheforfaitsTable->save($ficheforfait);


        }else if ($this->request->is('get')) {

            // Récupération des frais forfaitisés pour le mois en cours et pour l'utilisateur connecté en session
            $this->loadModel('Ficheforfaits');
            $fichesforfaits = $this->Ficheforfaits->find("all", [
                'fields' => array('fraisforfaits.id', 'Ficheforfaits.quantite', 'fraisforfaits.id_types', 'fraisforfaits.description', 'fraisforfaits.date_debut', 'fraisforfaits.date_fin', 'types.libelle', 'types.montant'),
                'conditions' => array('Fichefrais.id_user' => $user_id, 'Fichefrais.mois' => $mois),
                'join' => array(
                    array('table' => 'fichefrais',
                        'alias' => 'fichefrais',
                        'type' => 'LEFT',
                        'conditions' => array('fichefrais.id = Ficheforfaits.id_fichefrais')
                    ),
                    array('table' => 'fraisforfaits',
                        'alias' => 'fraisforfaits',
                        'type' => 'LEFT',
                        'conditions' => array('fraisforfaits.id = Ficheforfaits.id_fraisforfaits')
                    ),
                    array('table' => 'types',
                        'alias' => 'types',
                        'type' => 'LEFT',
                        'conditions' => array('types.id = Fraisforfaits.id_types')
                    )
                )
            ]);

            $data_forfaits = array();
            foreach ($fichesforfaits as $key => $fraisforfait) {
                //$data_forfaits[$key]['Type'] =  $fraisforfait['types']['libelle'];
                $data_forfaits[$key]['Type'] = $fraisforfait['fraisforfaits']['id_types'];
                $data_forfaits[$key]['ID'] = $fraisforfait['fraisforfaits']['id'];
                $data_forfaits[$key]['Description'] = $fraisforfait['fraisforfaits']['description'];
                $data_forfaits[$key]['Debut'] = $fraisforfait['fraisforfaits']['date_debut'];
                $data_forfaits[$key]['Fin'] = $fraisforfait['fraisforfaits']['date_fin'];
                $data_forfaits[$key]['Quantite'] = $fraisforfait['quantite'];
                $data_forfaits[$key]['PU'] = $fraisforfait['types']['montant'];
                $montant = $fraisforfait['quantite'] * $fraisforfait['types']['montant'];
                $data_forfaits[$key]['Montant'] = $montant;
            }
            //debug($data_forfaits); die();
            echo json_encode($data_forfaits);
            exit();
            //return $data_forfaits;

            $this->set('data_forfaits', $data_forfaits);

            // Récupération du statut et du montant validé de la fiche de frais en cours
            $this->loadModel('Fichefrais');
            $this->set('statut_fiche_frais', $this->Fichefrais->getStatutFichefrais($user_id, $mois));
            $this->set('montantValide_fiche_frais', $this->Fichefrais->getMontantValide($user_id, $mois));
            $this->set('id_fichefrais', $this->Fichefrais->getId($user_id, $mois));
            $this->set('mois', $mois);

        }else if ($this->request->is('post')) {
            $data = $this->request->data;

            $FraisforfaitsTable = TableRegistry::get('Fraisforfaits');
            $fraisforfait = $FraisforfaitsTable->newEntity();
            $fraisforfait->description = $data['Description'];
            $fraisforfait->date_debut = $data['Debut'];
            $fraisforfait->date_fin = $data['Fin'];
            $fraisforfait->id_types = $data['Type'];
            $save_fraisforfaits = $FraisforfaitsTable->save($fraisforfait);

            $this->loadModel('Fichefrais');
            $id_fichefrais = $this->Fichefrais->find("all", [
                'fields' => array('id'),
                'conditions' => array(
                    'Fichefrais.id_user' => $user_id,
                    'Fichefrais.mois' => $mois
                )
            ])->first();

            $FicheforfaitsTable = TableRegistry::get('Ficheforfaits');
            $ficheforfait = $FicheforfaitsTable->newEntity();
            $ficheforfait->quantite = $data['Quantite'];
            $ficheforfait->id_fichefrais = $id_fichefrais->id;
            $ficheforfait->id_fraisforfaits = $save_fraisforfaits->id;
            $FicheforfaitsTable->save($ficheforfait);

            echo json_encode('OK');
            exit();

        }else if ($this->request->is('delete')) {
            $data = $this->request->data;

            $this->loadModel('Fraisforfaits');
            $entity = $this->Fraisforfaits->get($data['ID']);
            $result = $this->Fraisforfaits->delete($entity);

            $this->loadModel('Fichefrais');
            $id_fichefrais = $this->Fichefrais->find("all", [
                'fields' => array('id'),
                'conditions' => array(
                    'Fichefrais.id_user' => $user_id,
                    'Fichefrais.mois' => $mois
                )
            ])->first();

            $this->loadModel('Ficheforfaits');
            $this->Ficheforfaits->deleteAll(['id_fraisforfaits' => $data['ID'], 'id_fichefrais' => $id_fichefrais->id]);
        }

    }


}
