<?php

namespace App\Controller\Component;
use Cake\Controller\Component;

class FonctionsComponent extends Component{

    /**
     * Transforme une date au format français jj/mm/aaaa vers le format anglais aaaa-mm-jj
     * @param $madate au format  jj/mm/aaaa
     * @return la date au format anglais aaaa-mm-jj
     */
    function dateFrancaisVersAnglais($maDate){
        @list($jour,$mois,$annee) = explode('/',$maDate);
        return date('Y-m-d',mktime(0,0,0,$mois,$jour,$annee));
    }

}