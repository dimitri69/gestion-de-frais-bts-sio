<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FicheforfaitsFixture
 *
 */
class FicheforfaitsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'quantite' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'id_fichefrais' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'id_fraisforfaits' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'FK_ficheforfaits_id_fraisforfaits' => ['type' => 'index', 'columns' => ['id_fraisforfaits'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id_fichefrais', 'id_fraisforfaits'], 'length' => []],
            'FK_ficheforfaits_id_fraisforfaits' => ['type' => 'foreign', 'columns' => ['id_fraisforfaits'], 'references' => ['fraisforfaits', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'FK_ficheforfaits_id_fichefrais' => ['type' => 'foreign', 'columns' => ['id_fichefrais'], 'references' => ['fichefrais', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'quantite' => 1,
            'id_fichefrais' => 1,
            'id_fraisforfaits' => 1
        ],
    ];
}
